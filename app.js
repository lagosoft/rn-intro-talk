import React, { Component } from 'react'
import { View } from 'react-native'
import { BrowserRouter } from 'react-router-dom'
import Main from './src/Main'

export default class DemoableApp extends Component {
  render() {
    return (
      <BrowserRouter>
          <Main/>
      </BrowserRouter>
    )
  }
}
