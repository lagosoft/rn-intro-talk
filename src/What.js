import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

const What = ({nav, history, back, fwd, styles}) => {
    return (
        <View style={styles.container}
              onClick={() => nav(history, back, fwd)}
        >
        <Text style={styles.title}>What will we 
            <Text style={[styles.title, {fontWeight: '700'}]}>cover?</Text></Text>
        <Text style={styles.title}>{`
            Preparing our machines:
                node
                create-react-native-app
                expo app install
                deploy demo app to phone
                making updates
                how to continue learning
                troubleshoot
            `}</Text>
        </View>
    );
};

export default What;
