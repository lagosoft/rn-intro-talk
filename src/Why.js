import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

additionalText = [
    'It\'s not a Hybrid framework',
    'Immediate developer feedback = faster iteration',
    'Delivers true Native apps iOS, Android',
    'Native everywhere: Web, tv-iOS, OSX, Win, Ubuntu',
    'With Expo: deploy (wireless) dev builds to phone',
    '& share app without going through Apple/Google stores'
    
]

class Why extends Component {
    constructor(props){
        super(props)
        this.state = {
            clicks: 0
        }
    }

    render() {

        additional = [];
        for(let i=0; i<this.state.clicks; i++){
            additional.push(
                <Text 
                    style={i===this.state.clicks-1 
                            ? this.props.styles.newItem
                            : this.props.styles.text} 
                    key={i}>
                        {additionalText[i]}
                </Text>
            )
        }
        
        handleClick = (nav, history, back, fwd, styles) => {
            if (this.state.clicks < additionalText.length) {
                this.setState((prevState) => { clicks: prevState.clicks++})
            }
            else {
                this.props.nav(history, back,fwd)
            }
        }

        return (
            <View style={this.props.styles.container}
                onClick={() => handleClick(this.props.nav, this.props.history, this.props.back, this.props.fwd, this.props.styles)}
            >
                <Text style={this.props.styles.title}>
                    <Text style={{fontStyle: 'italic', fontWeight: '900'}}>Why</Text> react-native?
                </Text>
                { additional }
            </View>
        );
    }
};

export default Why;
