import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'

const Welcome = ({nav, history, back, fwd, styles}) => {
    return (
        <View 
            style={styles.container} 
            onClick={() => nav(history, back, fwd)}>
            <Text style={styles.title}>
            An intro to <Text style={{fontWeight: '900'}}>React Native</Text>
            </Text>
            <Text style={styles.text}>
            and Expo!
            </Text>
        </View> 
    )
}

export default Welcome
