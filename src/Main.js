import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import { Switch, Route } from 'react-router-dom'
import { withRouter } from 'react-router-dom'

import Welcome from './Welcome'
import Why from './Why'
import What from './What'

META_KEY_DOWN = false

window.onkeydown = (e) => {
    META_KEY_DOWN = e.keyCode === 91 || e.keyCode === 93
}

window.onkeyup = () => {
    META_KEY_DOWN = false
}

nav = (history, back, fwd) => {
    path = META_KEY_DOWN ? back : fwd
    history.push(path)
}

class Main extends Component {
    constructor(props){
        super(props)
    }

    render() {

        return (
            <Switch>
                <Route exact path='/' 
                       render={ (props) =>
                       <Welcome {...props} nav={nav} styles={styles} back='/what' fwd='/why' />}/>
                <Route path='/why' 
                        render={(props) => 
                        <Why {...props} nav={nav} styles={styles} back='/' fwd='/what' />}/>
                <Route path='/what' 
                        render={(props) => 
                        <What {...props} nav={nav} styles={styles} back='/why' fwd='/' />}/>
            </Switch>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#000',
    },
    title: {
        color: '#FFF',
        fontSize: 60,
        margin: 10,
    },
    text: {
        marginBottom: 5,
        fontSize: 35,
        fontStyle: 'italic',
        color: 'lightblue'
    },
    newItem: {
        marginBottom: 5,
        fontSize: 55,
        color: 'orange'
    }
})

export default Main