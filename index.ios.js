/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import { AppRegistry } from 'react-native';
import DemoableApp from './app'

AppRegistry.registerComponent('ExampleApp', () => DemoableApp);
